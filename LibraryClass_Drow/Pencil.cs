﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace LibraryClass_Drow
{
    public class Pencil
    {
        public Pen pen = new Pen(Color.Black, 3);
        public int Mode = 0; // 0 - pen 1 - raber 2 - fill 3 - rectangle 4 - elipse 5 - переміщення
        public SolidBrush brush = new SolidBrush(Color.White);
        private Color ffolor;
        public Color FColor
        {
            get { return ffolor; }
            set
            {
                if (ffolor != value)
                {
                    ffolor = value;
                    pen.Color = value;
                }

            }
        }
        public int pensize { get { return (int)pen.Width; } set { pen.Width = value; } }
        public Pencil(int mod)
        {
            if ((mod >= 0) && (mod <= 5))
                Mode = mod;
        }
        public Pencil()
        {
            Mode = 0;
        }
    }
}
