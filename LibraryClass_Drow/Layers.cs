﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using LibraryClass_Drow;
using Microsoft.VisualBasic;

namespace LibraryClass_Drow
{

    public class Layer
    {
        public Pencil pencil;
        public string name;
        public Bitmap bmp;
        private int fopacity = 100;
        //Color ffolor;
        public int opacity
        {
            get { return fopacity; }
            set
            {
                if (fopacity != value)
                {
                    fopacity = value;
                }

            }
        }
        public bool visible = true;
        public bool locked = true;
        //public Color FColor
        //{
        //    get { return ffolor; }
        //    set
        //    {
        //        if (ffolor != value)
        //        {
        //            ffolor = value;
        //            pen.Color = value;
        //        }

        //    }
        //}
        //public int pensize { get { return (int)pen.Width; } set { pen.Width = value; } }
        //private Pen pen = new Pen(Color.Black, 3);
        
        public Point OldPoint = new Point(0, 0);
        //public int Mode = 0; // 0 - pen 1 - raber 2 - fill 3 - rectangle 4 - elipse 5 - переміщення
        public float scaleX = 1, scaleY = 1;
        public Layer(int width, int height,int mod)
        {
            bmp = new Bitmap(width, height);
            pencil = new Pencil(mod);
        }
        //public void Remove(Point p, Point size) // для круглої гумки
        //{
        //    Point point = new Point((int)(p.X * scaleX), (int)(p.Y * scaleY));
        //    //(x - x0)^2 + (y - y0)^2 <= R^2     x і y - координаты вашей точки, x0 и y0 - координаты центра
        //    for (int i = (point.X - (size.X / 2)); i <= (point.X + (size.X / 2)); i++)
        //    {
        //        for (int j = (point.Y - (size.Y / 2)); j <= (point.Y + (size.Y / 2)); j++)
        //        {
        //            if (Math.Pow((i - point.X), 2) + Math.Pow((j - point.Y), 2) <= Math.Pow((size.X / 2.0), 2))
        //            {
        //                if (((i >= 0) && (i < bmp.Width) && (j >= 0) && (j < bmp.Height)))
        //                {
        //                    bmp.SetPixel(i, j, Color.Transparent);
        //                }
        //            }
        //        }
        //    }
        //}
        private void DrLine(Point pointb, Point pointe)
        {
            Point point2 = new Point((int)(scaleX * pointe.X), (int)(scaleY * pointe.Y));
            Point point1 = new Point((int)(scaleX * pointb.X), (int)(scaleY * pointb.Y));

            if ((point2.X <= 0) || (point1.X <= 0) || (point2.Y <= 0) || (point1.Y <= 0) ||
                (point2.X >= bmp.Width) || (point1.X >= bmp.Width) || (point2.Y >= bmp.Height) || (point1.Y >= bmp.Height))
            {
                return;
            }

            Graphics.FromImage(bmp).DrawLine(pencil.pen, point1, point2);
        }
        //public Point wp2dp(Point wp)
        //{
        //    return new Point((int)wp.X, (int)wp.Y);
        //}
        public void MoveTo(Point p)
        {
            OldPoint = p;
        }
        public void LineTo(Point p)
        {
            DrLine(OldPoint, p);
            OldPoint = p;
        }
        public override string ToString()
        {
            return name;
        }

        public static Image ChangeImageOpacity(Image originalImage, double opacity)
        {
            byte bytesPerPixel = 4;
            if ((originalImage.PixelFormat & PixelFormat.Indexed) == PixelFormat.Indexed)
            {
                // Cannot modify an image with indexed colors
                return originalImage;
            }
            
            Bitmap bmp = (Bitmap)originalImage.Clone();

            // Specify a pixel format.
            PixelFormat pxf = PixelFormat.Format32bppArgb;

            // Lock the bitmap's bits.
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            // This code is specific to a bitmap with 32 bits per pixels 
            // (32 bits = 4 bytes, 3 for RGB and 1 byte for alpha).
            int numBytes = bmp.Width * bmp.Height * bytesPerPixel;
            byte[] argbValues = new byte[numBytes];

            // Copy the ARGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, argbValues, 0, numBytes);

            // Manipulate the bitmap, such as changing the
            // RGB values for all pixels in the the bitmap.
            for (int counter = 0; counter < argbValues.Length; counter += bytesPerPixel)
            {
                // argbValues is in format BGRA (Blue, Green, Red, Alpha)

                // If 100% transparent, skip pixel
                if (argbValues[counter + bytesPerPixel - 1] == 0)
                    continue;

                int pos = 0;
                pos++; // B value
                pos++; // G value
                pos++; // R value

                argbValues[counter + pos] = (byte)(argbValues[counter + pos] * opacity);
            }

            // Copy the ARGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(argbValues, 0, ptr, numBytes);

            // Unlock the bits.
            bmp.UnlockBits(bmpData);

            return bmp;
        }
        public Image OpacBMP()
        {
            return ChangeImageOpacity(bmp, opacity / 100.0);
        }
        public void DrawPoint(Point e) // установка точки
        {
            Graphics.FromImage(bmp).FillEllipse(new SolidBrush(pencil.FColor), (float)(e.X * scaleX - (pencil.pensize / 2)), (float)(e.Y * scaleY - (pencil.pensize / 2)), pencil.pensize, pencil.pensize);
        }
        //public void DrRactPeint(Point pointb, Point pointe, Bitmap Nbmp)
        //{
        //    Point point2 = new Point((int)(scaleX * pointe.X), (int)(scaleY * pointe.Y)),
        //       point1 = new Point((int)(scaleX * pointb.X), (int)(scaleY * pointb.Y));

        //    if ((point2.X <= 0) || (point1.X <= 0) || (point2.Y <= 0) || (point1.Y <= 0) ||
        //        (point2.X >= Nbmp.Width) || (point1.X >= Nbmp.Width) || (point2.Y >= Nbmp.Height) || (point1.Y >= Nbmp.Height))
        //    {
        //        return;
        //    }
        //    Graphics.FromImage(Nbmp).DrawRectangle(pen, point1.X, point1.Y, point2.X, point2.Y);

        //}
        //public void DrRact(Bitmap BMP, Point pointb, Point pointe)
        //{
        //    Point point2 = new Point((int)(scaleX * pointe.X), (int)(scaleY * pointe.Y)),
        //       point1 = new Point((int)(scaleX * pointb.X), (int)(scaleY * pointb.Y));

        //    if ((point2.X <= 0) || (point1.X <= 0) || (point2.Y <= 0) || (point1.Y <= 0) ||
        //        (point2.X >= bmp.Width) || (point1.X >= bmp.Width) || (point2.Y >= bmp.Height) || (point1.Y >= bmp.Height))
        //    {
        //        return;
        //    }
        //    Graphics.FromImage(BMP).DrawRectangle(pen, point1.X, point1.Y, point2.X, point2.Y);

        //}
        //public void DrEllips(Point pointb, Point pointe)
        //{
        //    Point point2 = new Point((int)(scaleX * pointe.X), (int)(scaleY * pointe.Y)),
        //       point1 = new Point((int)(scaleX * pointb.X), (int)(scaleY * pointb.Y));

        //    if ((point2.X <= 0) || (point1.X <= 0) || (point2.Y <= 0) || (point1.Y <= 0) ||
        //        (point2.X >= bmp.Width) || (point1.X >= bmp.Width) || (point2.Y >= bmp.Height) || (point1.Y >= bmp.Height))
        //    {
        //        return;
        //    }
        //    Graphics.FromImage(bmp).DrawEllipse(pen, point1.X, point1.Y, point2.X, point2.Y);

        //}
        public void floodfill(Point p, Color replacementColor)
        {

            Point pl = new Point((int)(p.X * scaleX), (int)(p.Y * scaleY));
            Color tg = bmp.GetPixel(pl.X, pl.Y);
            floodfillA(pl, tg, replacementColor);

        }
        private void floodfillA(Point p, Color tg, Color replacementColor)
        {
            Point pt = p;
            Stack<Point> pixels = new Stack<Point>();
            pixels.Push(pt);
            while (pixels.Count > 0)
            {
                Point a = pixels.Pop();
                if (a.X < bmp.Width && a.X >= 0 &&
                        a.Y < bmp.Height && a.Y >= 0) //перевірка на границі
                {
                    Color tcl = bmp.GetPixel(a.X, a.Y);
                    if ((tcl.ToArgb() == tg.ToArgb()) && (tcl.ToArgb() != replacementColor.ToArgb()))
                    {
                        bmp.SetPixel(a.X, a.Y, replacementColor);
                        pixels.Push(new Point(a.X - 1, a.Y));
                        pixels.Push(new Point(a.X + 1, a.Y));
                        pixels.Push(new Point(a.X, a.Y - 1));
                        pixels.Push(new Point(a.X, a.Y + 1));
                    }
                }
            }
            return;
        }
        public void SetRectColor(Bitmap bmp, int x, int y, int w, Color cl)
        {
            int xx, yy;
            xx = (int)(scaleX * x);
            yy = (int)(scaleY * y);
            for (int i = xx - w / 2; i < xx + w / 2; i++)
                for (int j = yy - w / 2; j < yy + w / 2; j++)
                    if (i >= 0 && i < bmp.Width && j >= 0 && j < bmp.Height)
                        bmp.SetPixel(i, j, cl);
        }

        public void DrRuber(Point e)
        {
            if (e.X - OldPoint.X != 0)
            {
                double k = (double)(e.Y - OldPoint.Y) / (double)(e.X - OldPoint.X);
                if (e.X > OldPoint.X)
                    for (int i = 0; i < 4 * (e.X - OldPoint.X); i++)
                        SetRectColor(bmp,
                                        (i / 4) + OldPoint.X,
                                        (int)((i * k) / 4) + OldPoint.Y,
                                        (int)pencil.pensize,
                                        Color.Transparent);

                if (e.X < OldPoint.X)
                    for (int i = 0; i > 4 * (e.X - OldPoint.X); i--)
                        SetRectColor(bmp, (i / 4) + OldPoint.X,
                                        (int)((i * k) / 4) + OldPoint.Y,
                                        (int)pencil.pensize,
                                        Color.Transparent);
            }
            else
            {
                int step = 1;
                if (e.Y < OldPoint.Y) step = -1;
                for (int i = OldPoint.Y; i != e.Y; i = i + step)
                    SetRectColor(bmp, OldPoint.X, i,
                                    (int)pencil.pensize,
                                    Color.Transparent);
            }
            OldPoint = e;
        }
        public void DrawRect(Bitmap BMP,Point OldPoint,Point e)
        {
            if (e.X < OldPoint.X && e.Y < OldPoint.Y)
            {
                Point p1 = e;
                Point p2 = OldPoint;
                e = p2;
                OldPoint = p1;
                Graphics.FromImage(BMP).DrawRectangle(pencil.pen, scaleX * OldPoint.X, scaleY * OldPoint.Y, e.X * scaleX - OldPoint.X * scaleX, e.Y * scaleY - OldPoint.Y * scaleY);
                return;
            }
            else
            {
                Graphics.FromImage(BMP).DrawRectangle(pencil.pen, scaleX * OldPoint.X, scaleY * OldPoint.Y, e.X * scaleX - OldPoint.X * scaleX, e.Y * scaleY - OldPoint.Y * scaleY);
            }
            if (e.X > OldPoint.X && e.Y < OldPoint.Y)
            {
                Point p1 = e;
                e.Y = OldPoint.Y;
                OldPoint.Y = p1.Y;
                Graphics.FromImage(BMP).DrawRectangle(pencil.pen, scaleX * OldPoint.X, scaleY * OldPoint.Y, e.X * scaleX - OldPoint.X * scaleX, e.Y * scaleY - OldPoint.Y * scaleY);
                return;
            }
            if (e.X < OldPoint.X && e.Y > OldPoint.Y)
            {
                Point p1 = e;
                e.X = OldPoint.X;
                OldPoint.X = p1.X;
                Graphics.FromImage(BMP).DrawRectangle(pencil.pen, scaleX * OldPoint.X, scaleY * OldPoint.Y, e.X * scaleX - OldPoint.X * scaleX, e.Y * scaleY - OldPoint.Y * scaleY);
                return;
            }

        }
        public void DrawEllip(Bitmap BMP, Point OldPoint, Point e)
        {
            Graphics.FromImage(BMP).DrawEllipse(pencil.pen, scaleX * OldPoint.X, scaleY * OldPoint.Y, e.X * scaleX - OldPoint.X * scaleX, e.Y * scaleY - OldPoint.Y * scaleY);
        }
        public void DrawLin(Bitmap BMP, Point OldPoint, Point e)
        {
            Graphics.FromImage(BMP).DrawLine(pencil.pen, scaleX * OldPoint.X, scaleY * OldPoint.Y, e.X * scaleX, e.Y * scaleY);
        }
    }

}
