﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using LibraryClass_Drow;
using Microsoft.VisualBasic;

namespace Mein_Wind
{

    public partial class MeinWindow : Form
    {
        public Point bmpsize = new Point(640, 480); // Розміри слою
        private bool richSaveOpen = true; // для вирішення чи зберігати фон
        private string fname = ""; 
        private Bitmap prevBmp = new Bitmap(640, 480), nextBmp = new Bitmap(640, 480);
        private Bitmap FBMP = new Bitmap(640, 480); // Віртуальний слой
        private PointF PBScale = new PointF(1, 1);  // Скейл холста
        private Bitmap bgbmp = new Bitmap(640, 480); // фон задніх клітинок
        int OldMod = 0;
        public MeinWindow()
        {
            InitializeComponent();
        }
        private void FastPBDraw(Bitmap Fbmp) // перемальовка слоїв 
        {
            var layer = (LB.SelectedItem as Layer);
            Bitmap bmp = new Bitmap(bmpsize.X, bmpsize.Y);
            if(richSaveOpen)
                Graphics.FromImage(bmp).DrawImage(bgbmp, 0, 0, bmpsize.X, bmpsize.Y); // холст 
            if (prevBmp != null)
                Graphics.FromImage(bmp).DrawImage(prevBmp, 0, 0, bmpsize.X, bmpsize.Y); // задні слої
            if (layer != null)
                if(layer.visible)
                Graphics.FromImage(bmp).DrawImage(layer.OpacBMP(), 0, 0, bmpsize.X, bmpsize.Y); // вибраний сло1
            if (Fbmp != null)
                Graphics.FromImage(bmp).DrawImage(Fbmp, 0, 0, Fbmp.Width, Fbmp.Height); // Віртуальний 
            if (nextBmp != null)
                Graphics.FromImage(bmp).DrawImage(nextBmp, 0, 0, bmpsize.X, bmpsize.Y); // Верхні слої
            PB.Image = new Bitmap(bmp);
            PB.Refresh();
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            var lay = (LB.SelectedItem as Layer);
            if (lay == null)
                return;
            if (PB.Image != null) 
                PB.Image.Dispose(); 
            FastPBDraw(null);
        }
        private void PB_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;
            Layer lay = LB.SelectedItem as Layer;
            if (!lay.visible)
                return;
            lay.MoveTo(e.Location); // зброс останньої точки та установка при нажатты
            if (LB.SelectedIndex >= 0)
            {
                (LB.SelectedItem as Layer).locked = false;
            }
            if (!lay.locked)
            {
                switch (lay.pencil.Mode)
                {
                    case 0:
                        {
                            lay.DrawPoint(e.Location);
                        }
                        break;
                    case 1: //гумка
                        {
                            //FBMP = new Bitmap(bmpsize.X, bmpsize.Y);
                            lay.SetRectColor(lay.bmp, e.X, e.Y, (int)lay.pencil.pensize, Color.Transparent);
                            //Graphics.FromImage(FBMP).DrawRectangle(new Pen(Color.Red, 1), new Rectangle(e.X - (int)lay.pensize / 2, e.Y - (int)lay.pensize / 2, (int)lay.pensize, (int)lay.pensize));
                        }
                        break;
                    case 2: //Залівка
                        {
                            lay.floodfill(e.Location, Col.BackColor);
                        }
                        break;
                    case 3: // Кавдрат
                        {
                            //lay.OldPoint = e.Location;
                        }
                        break;
                    case 4: // Коло
                        {
                            //lay.OldPoint = e.Location;
                        }
                        break;
                    case 5: // Переміщення
                        {
                            //lay.OldPoint = e.Location;
                        }
                        break;
                    case 6: // Перяма лінія 
                        {
                            //lay.OldPoint = e.Location;
                        }
                        break;
                    default:
                        break;
                }
            }
            Refresh();
        }

        private void PB_MouseMove(object sender, MouseEventArgs e)
        {
            if (LB.SelectedItem != null)
            {
                Layer lay = LB.Items[LB.SelectedIndex] as Layer;
                if (!lay.visible)
                    return;
                if (!lay.locked)
                {
                    switch (lay.pencil.Mode)
                    {
                        case 0:
                            {
                                FBMP = new Bitmap(bmpsize.X, bmpsize.Y);
                                Graphics.FromImage(FBMP).DrawEllipse(new Pen(Color.Red, 1),
                                                   new Rectangle((int)((e.X * lay.scaleX) - (lay.pencil.pensize / 2)),
                                                                 (int)((e.Y * lay.scaleY) - (lay.pencil.pensize / 2)),
                                                                 (int)(lay.pencil.pensize),
                                                                 (int)(lay.pencil.pensize)));
                                lay.DrawPoint(e.Location);
                                lay.LineTo(e.Location);
                                FastPBDraw(FBMP);
                                FBMP = null;
                            }
                            break;
                        case 1: //гумка
                            {
                                FBMP = new Bitmap(bmpsize.X, bmpsize.Y);
                                Graphics.FromImage(FBMP).DrawRectangle(new Pen(Color.Red, 1),
                                                   new Rectangle((int)((e.X * lay.scaleX) - (lay.pencil.pensize / 2)),
                                                                 (int)((e.Y * lay.scaleY) - (lay.pencil.pensize / 2)),
                                                                 (int)(lay.pencil.pensize),
                                                                 (int)(lay.pencil.pensize)));
                                lay.DrRuber(e.Location);
                                FastPBDraw(FBMP);
                                FBMP = null;
                            }
                            break;
                        case 3: // Квадват
                            {
                                FBMP = new Bitmap(bmpsize.X, bmpsize.Y);
                                lay.DrawRect(FBMP, lay.OldPoint, e.Location);
                                FastPBDraw(FBMP);
                                FBMP = null;
                            }
                            break;
                        case 4: // Круг
                            {
                                FBMP = new Bitmap(bmpsize.X, bmpsize.Y);
                                lay.DrawEllip(FBMP, lay.OldPoint, e.Location);
                                FastPBDraw(FBMP);
                                FBMP = null;
                            }
                            break;
                        case 5: // переміщення
                            {
                                FBMP = new Bitmap(bmpsize.X, bmpsize.Y);
                                Graphics.FromImage(FBMP).DrawImage(lay.OpacBMP(), (e.X * PBScale.X + 1) - lay.OldPoint.X*PBScale.X, (e.Y * PBScale.Y + 1) - lay.OldPoint.Y*PBScale.Y, bmpsize.X, bmpsize.Y);
                                Pen dashpen = new Pen(Color.Red, 2);
                                float[] dp = { 8, 4 };
                                dashpen.DashPattern = dp;
                                Graphics.FromImage(FBMP).DrawRectangle(dashpen, (e.X * PBScale.X + 1) - lay.OldPoint.X* PBScale.X, (e.Y * PBScale.Y + 1) - lay.OldPoint.Y* PBScale.Y, bmpsize.X, bmpsize.Y);
                                FastPBDraw(FBMP);
                                FBMP = null;
                            }
                            break;
                        case 6: // Лінія
                            {
                                FBMP = new Bitmap(bmpsize.X, bmpsize.Y);
                                lay.DrawLin(FBMP, lay.OldPoint, e.Location);
                                FastPBDraw(FBMP);
                                FBMP = null;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void PB_MouseUp(object sender, MouseEventArgs e)
        {
            Layer lay = LB.SelectedItem as Layer;
            if (LB.SelectedItem != null)
            {
                if (!lay.visible)
                    return;
                if (!lay.locked)
                {
                    switch (lay.pencil.Mode)
                    {
                        case 0: //карандаш
                            {
                                FBMP = null;
                            }break;
                        case 1: // гумка
                            {
                                FBMP = null;
                            }
                            break;
                        case 3: // Квадват
                            {
                                FBMP = null;
                                lay.DrawRect(lay.bmp, lay.OldPoint, e.Location);                            
                            }
                            break;
                        case 4: // Круг
                            {
                                FBMP = null;
                                lay.DrawEllip(lay.bmp, lay.OldPoint, e.Location);
                            }
                            break;
                        case 5: // переміщення
                            {
                                FBMP = new Bitmap(bmpsize.X, bmpsize.Y);
                                Graphics.FromImage(FBMP).DrawImage(lay.OpacBMP(), (e.X - lay.OldPoint.X) * lay.scaleX, (e.Y - lay.OldPoint.Y) * lay.scaleY, bmpsize.X, bmpsize.Y);
                                lay.bmp = FBMP;
                                FBMP = null;
                            }
                            break;
                        case 6: // Лінія
                            {
                                FBMP = null;
                                lay.DrawLin(lay.bmp, lay.OldPoint, e.Location);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            if (LB.SelectedIndex >= 0)  // блокування слою
            {
                if (lay != null) 
                {
                    lay.locked = true;
                }
            }
            Refresh();
        }
        private void B_UP_Click(object sender, EventArgs e) { UpLayer(); }
        private void UpLayer()
        {
            int i = LB.SelectedIndex;
            if (i > 0)
            {
                LB.SelectedIndexChanged -= LB_SelectedIndexChanged;
                LB.ItemCheck -= LB_ItemCheck;
                bool check = LB.GetItemChecked(i);
                LB.BeginUpdate();
                LB.Items.Insert(i - 1, LB.SelectedItem);
                LB.SelectedIndex = i - 1;
                LB.SetItemChecked(i - 1, check);
                LB.Items.RemoveAt(i + 1);
                LB.EndUpdate();
                LB.SelectedIndexChanged += LB_SelectedIndexChanged;
                LB.ItemCheck += LB_ItemCheck;
                //FastPBDraw(null);
            }
        }
        private void B_Down_Click(object sender, EventArgs e) { DownLayer(); }
        private void DownLayer()
        {
            int i = LB.SelectedIndex;
            
            if (i + 1 < LB.Items.Count)
            {
                
                LB.SelectedIndexChanged -= LB_SelectedIndexChanged;
                LB.ItemCheck -= LB_ItemCheck;
                bool check = LB.GetItemChecked(i);
                LB.BeginUpdate();
                LB.Items.Insert(i + 2, LB.SelectedItem);
                LB.Items.RemoveAt(i);
                LB.SelectedIndex = i + 1;
                LB.SetItemChecked(i + 1, check);
                LB.EndUpdate();
                LB.SelectedIndexChanged += LB_SelectedIndexChanged;
                LB.ItemCheck += LB_ItemCheck;
                //FastPBDraw(null);
            }
        }
        private void Col_Click(object sender, EventArgs e)
        {
            if (ColDialog.ShowDialog() == DialogResult.OK)
            {
                if ((LB.Items[LB.SelectedIndex] as Layer) != null)
                {
                    (LB.Items[LB.SelectedIndex] as Layer).pencil.FColor = ColDialog.Color;
                    ((Button)sender).BackColor = ColDialog.Color;
                }
            }
        }
        private void OP_ValueChanged(object sender, EventArgs e)
        {
            if (OP.Value < 1)
            {
                OP.Value = 100;
                MessageBox.Show("Помилка, значення має бути >0 ", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            (LB.SelectedItem as Layer).opacity = (int)OP.Value; // прозорість слою
            Refresh();
        }
        private void LB_Click(object sender, EventArgs e)
        {
            var lay = (LB.SelectedItem as Layer); // Встановка параметрів
            lay.visible = (LB.CheckedItems.IndexOf(LB.SelectedItem)>=0);
            OP.Value = lay.opacity;
            Col.BackColor = lay.pencil.FColor;
            TB.Value = lay.pencil.pensize;
            SIZEP.Value = lay.pencil.pensize;
            //FastPBDraw(null);
        }
        private void ADD_Click(object sender, EventArgs e) { AddLayer(); }
         private void AddLayer()
        {
            
            LB.SelectedIndexChanged -= null;
            Layer lay = new Layer(bmpsize.X, bmpsize.Y, OldMod);
            lay.name = "Шар" + LB.Items.Count.ToString();
            lay.scaleX = PBScale.X;
            lay.scaleY = PBScale.Y;
            LB.BeginUpdate();
            LB.Items.Insert(0, lay);
            LB.SelectedIndex = 0;
            LB.SetItemChecked(0, true);
            LB.EndUpdate();
            lay.pencil.FColor = Col.BackColor;
            lay.pencil.pensize = TB.Value;
            lay.visible = true;
            LB.SelectedIndexChanged += LB_SelectedIndexChanged; 
        }
        private void Pen_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.Mode = 0;
            OldMod = 0;
        }
        private void Raber_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.Mode = 1;
            OldMod = 1;
        }
        private void FILL_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.Mode = 2;
            OldMod = 2;
        }
        private void Rectang_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.Mode = 3;
            OldMod = 3;
        }
        private void Elipse_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.Mode = 4;
            OldMod = 4;
        }
        private void B_Move_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.Mode = 5;
            OldMod = 5;
        }
        private void LineBtn_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.Mode = 6;
            OldMod = 6;
        }
        private void SIZEP_ValueChanged(object sender, EventArgs e)
        {
            
            if (SIZEP.Value < 1)
            {
                SIZEP.Value = 1;
                MessageBox.Show("Помилка, значення має бути >0 ", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            var lay = LB.SelectedItem as Layer;
            TB.Value = (int)SIZEP.Value;
            lay.pencil.pensize = int.Parse(SIZEP.Text);
            
        }
        private void TB_ValueChanged(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.pensize = TB.Value;
            SIZEP.Value = TB.Value;
        }
        private void save(string FN)
        {
            if (PB.Image != null)
            {
                
                PB.Image.Save(FN, ImageFormat.Png);
                fname = FN;
                
            }
        }
        private void зберегтиЯкToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richSaveOpen = false;
            FastPBDraw(null);
            SFD.Filter = "PNG(*.PNG)|*.png";
            if (SFD.ShowDialog() == DialogResult.OK)
            {
                save(SFD.FileName);
            }
            richSaveOpen = true;
            FastPBDraw(null);
        }
        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richSaveOpen = false;
            FastPBDraw(null);
            SFD.Filter = "PNG(*.PNG)|*.png";
            if (SFD.FileName=="")
            {
                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    save(SFD.FileName);
                }
            }
            else
            {
                save(SFD.FileName);
            }
            richSaveOpen = true;
            FastPBDraw(null);
        }
        private void відкритиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richSaveOpen = true;
            FastPBDraw(null);
            var l = (LB.SelectedItem as Layer);
            OFD.FileName = "";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                var mybmp = new BitmapImage();
                mybmp.BeginInit();
                mybmp.UriSource = new Uri(OFD.FileName);
                mybmp.CacheOption = BitmapCacheOption.OnLoad;
                mybmp.EndInit();
                var bmperev = new Bitmap(OFD.FileName);
                if (((int)bmperev.Width != bmpsize.X) && ((int)bmperev.Height != bmpsize.Y))
                {
                    MessageBox.Show($"Помилка, Зображення повинно бути {bmpsize.X}x{bmpsize.Y}", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                // Graphics.FromImage(lay.bmp).FillRectangle(Brushes.White, new RectangleF(0, 0, bmpsize.X, bmpsize.X));
                //bmpsize.X = (int)mybmp.Width;
                //bmpsize.Y = (int)mybmp.Height;
                //SizeCH();
                //Res();

                //lay.name = "Фото";
                //LB.Items.Add(lay);
                //LB.SelectedIndex = LB.SelectedIndex + 1;
                //lay.name = "Фото";
                //LB.SetItemChecked(0, true);
                LB.BeginUpdate();
                LB.SelectedIndexChanged -= null;
                var lay = new Layer(bmpsize.X, bmpsize.Y, l.pencil.Mode); ;
                lay.name = "Фото";
                lay.scaleX = (float)1.0 / ((float)PB.Width / (float)lay.bmp.Width); // зміна скейла
                lay.scaleY = (float)1.0 / ((float)PB.Height / (float)lay.bmp.Height);
                LB.Items.Insert(0, lay);
                LB.SelectedIndex = 0;
                LB.SetItemChecked(0, true);
                LB.EndUpdate();
                lay.pencil.FColor = Col.BackColor;
                lay.pencil.pensize = TB.Value;
                lay.visible = true;
                LB.SelectedIndexChanged += LB_SelectedIndexChanged;
                lay.bmp = new Bitmap(OFD.FileName);
                //bmpsize.X = (int)mybmp.Width; ((float)((float)((float)mybmp.Width / (float)bmpsize.X)))/((float)
                //bmpsize.Y = (int)mybmp.Height; ((float)((float)((float)mybmp.Height / (float)bmpsize.Y)))/((float)
                //lay.bmp = new Bitmap(ExtensionMethods.MyExtensions.BitmapImage2Bitmap(mybmp), (int)(bmpsize.X), (int)(bmpsize.Y));
                //Text = $"x = {lay.scaleX} y = {lay.scaleY}";
                //lay.scaleX = PBScale.X;
                //lay.scaleY = PBScale.Y;
                //SizeCH();
                //Res();
                //Text = $"x = {lay.scaleX} y = {lay.scaleY}";
                //lay.scaleX = lay.scaleX * (float)(PB.Width / (float)bmpsize.X);
                //lay.scaleY = lay.scaleY * (float)(PB.Height / (float)bmpsize.Y);
                fname = OFD.FileName;
                OFD.FileName = "";
                OFD.FileName = fname;
                Refresh();
            }
            else
            {
                //lay.bmp = new Bitmap(lay.bmp, bmpsize.X, bmpsize.Y);
                //Graphics.FromImage(lay.bmp).FillRectangle(Brushes.White, new RectangleF(0, 0, bmpsize.X, bmpsize.X));
                fname = "";
            }
        }

        private void ButDel_Click(object sender, EventArgs e) { DeleteLayer(); }
         private void DeleteLayer()
        {
            
            if (LB.SelectedItem == null || LB.Items.Count <= 1)
                return;
            int n = LB.SelectedIndex;
            LB.BeginUpdate();
            (LB.SelectedItem as Layer).bmp.Dispose();
            LB.Items.Remove(LB.SelectedItem);
            LB.EndUpdate();
            if (n > 0 && LB.Items.Count > 0)
            {
                n--;
            }
            LB.SelectedIndex = n;
            
        }
        private void VTimer_Tick(object sender, EventArgs e)
        {
            ButDel.Visible = LB.Items.Count > 1;
            if (LB.SelectedItem != null)
            {
                var lay = (LB.SelectedItem as Layer);
                lay.pencil.Mode = OldMod;
                lay.pencil.FColor = Col.BackColor;
                lay.pencil.pensize = TB.Value;
                switch (OldMod)
                {
                    case 0:
                        {
                            PenBtn.BackColor = Color.Gray;
                            RaberBtn.BackColor = Color.White;
                            FillBtn.BackColor = Color.White;
                            RectangBtn.BackColor = Color.White;
                            ElipseBtn.BackColor = Color.White;
                            B_MoveBtn.BackColor = Color.White;
                            LineBtn.BackColor = Color.White;

                            PenBtn.ForeColor = Color.Black;
                            RaberBtn.ForeColor = Color.White;
                            FillBtn.ForeColor = Color.White;
                            RectangBtn.ForeColor = Color.White;
                            ElipseBtn.ForeColor = Color.White;
                            B_MoveBtn.ForeColor = Color.White;
                            LineBtn.ForeColor = Color.White;
                        }
                        break;
                    case 1:
                        {
                            PenBtn.BackColor = Color.White;
                            RaberBtn.BackColor =  Color.Gray;
                            FillBtn.BackColor = Color.White;
                            RectangBtn.BackColor = Color.White;
                            ElipseBtn.BackColor = Color.White;
                            B_MoveBtn.BackColor = Color.White;
                            LineBtn.BackColor = Color.White;

                            PenBtn.ForeColor = Color.White;
                            RaberBtn.ForeColor = Color.Black;
                            FillBtn.ForeColor = Color.White;
                            RectangBtn.ForeColor = Color.White;
                            ElipseBtn.ForeColor = Color.White;
                            B_MoveBtn.ForeColor = Color.White;
                            LineBtn.ForeColor = Color.White;
                        }
                        break;
                    case 2:
                        {
                            PenBtn.BackColor = Color.White;
                            RaberBtn.BackColor = Color.White;
                            FillBtn.BackColor = Color.Gray;
                            RectangBtn.BackColor = Color.White;
                            ElipseBtn.BackColor = Color.White;
                            B_MoveBtn.BackColor = Color.White;
                            LineBtn.BackColor = Color.White;

                            PenBtn.ForeColor = Color.White;
                            RaberBtn.ForeColor = Color.White;
                            FillBtn.ForeColor = Color.Black;
                            RectangBtn.ForeColor = Color.White;
                            ElipseBtn.ForeColor = Color.White;
                            B_MoveBtn.ForeColor = Color.White;
                            LineBtn.ForeColor = Color.White;
                        }
                        break;
                    case 3:
                        {
                            PenBtn.BackColor = Color.White;
                            RaberBtn.BackColor = Color.White;
                            FillBtn.BackColor = Color.White;
                            RectangBtn.BackColor = Color.Gray;
                            ElipseBtn.BackColor = Color.White;
                            B_MoveBtn.BackColor = Color.White;
                            LineBtn.BackColor = Color.White;

                            PenBtn.ForeColor = Color.White;
                            RaberBtn.ForeColor = Color.White;
                            FillBtn.ForeColor = Color.White;
                            RectangBtn.ForeColor = Color.Black;
                            ElipseBtn.ForeColor = Color.White;
                            B_MoveBtn.ForeColor = Color.White;
                            LineBtn.ForeColor = Color.White;
                        }
                        break;
                    case 4:
                        {
                            PenBtn.BackColor = Color.White;
                            RaberBtn.BackColor = Color.White;
                            FillBtn.BackColor = Color.White;
                            RectangBtn.BackColor = Color.White;
                            ElipseBtn.BackColor = Color.Gray;
                            B_MoveBtn.BackColor = Color.White;
                            LineBtn.BackColor = Color.White;

                            PenBtn.ForeColor = Color.White;
                            RaberBtn.ForeColor = Color.White;
                            FillBtn.ForeColor = Color.White;
                            RectangBtn.ForeColor = Color.White;
                            ElipseBtn.ForeColor = Color.Black;
                            B_MoveBtn.ForeColor = Color.White;
                            LineBtn.ForeColor = Color.White;
                        }
                        break;
                    case 5:
                        {
                            PenBtn.BackColor = Color.White;
                            RaberBtn.BackColor = Color.White;
                            FillBtn.BackColor = Color.White;
                            RectangBtn.BackColor = Color.White;
                            ElipseBtn.BackColor = Color.White;
                            B_MoveBtn.BackColor = Color.Gray;
                            LineBtn.BackColor = Color.White;

                            PenBtn.ForeColor = Color.White;
                            RaberBtn.ForeColor = Color.White;
                            FillBtn.ForeColor = Color.White;
                            RectangBtn.ForeColor = Color.White;
                            ElipseBtn.ForeColor = Color.White;
                            B_MoveBtn.ForeColor = Color.Black;
                            LineBtn.ForeColor = Color.White;
                        }
                        break;
                    case 6:
                        {
                            PenBtn.BackColor = Color.White;
                            RaberBtn.BackColor = Color.White;
                            FillBtn.BackColor = Color.White;
                            RectangBtn.BackColor = Color.White;
                            ElipseBtn.BackColor = Color.White;
                            B_MoveBtn.BackColor = Color.White;
                            LineBtn.BackColor = Color.Gray;

                            PenBtn.ForeColor = Color.White;
                            RaberBtn.ForeColor = Color.White;
                            FillBtn.ForeColor = Color.White;
                            RectangBtn.ForeColor = Color.White;
                            ElipseBtn.ForeColor = Color.White;
                            B_MoveBtn.ForeColor = Color.White;
                            LineBtn.ForeColor = Color.Black;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        private void LB_SelectedIndexChanged(object sender, EventArgs e)
        {
            nextBmp = null;
            prevBmp = null;
            prevBmp = new Bitmap(bmpsize.X, bmpsize.Y);
            nextBmp = new Bitmap(bmpsize.X, bmpsize.Y);
            for (int i = LB.Items.Count - 1; i > LB.SelectedIndex; i--)
            {
                if (LB.Items[i] != null)
                {
                    var layer = (LB.Items[i] as Layer);
                    layer.scaleX = PBScale.X;
                    layer.scaleY = PBScale.Y;
                    if (layer.visible)
                        Graphics.FromImage(prevBmp).DrawImage(layer.OpacBMP(), 0, 0, bmpsize.X, bmpsize.Y);
                }
            }
            for (int i = LB.SelectedIndex - 1; i >= 0; i--)
            {
                if (LB.Items[i] != null)
                {
                    var layer = (LB.Items[i] as Layer);
                    layer.scaleX = PBScale.X;
                    layer.scaleY = PBScale.Y;
                    if (layer.visible)
                        Graphics.FromImage(nextBmp).DrawImage(layer.OpacBMP(), 0, 0, bmpsize.X, bmpsize.Y);
                }
            }
            Refresh();
        }
        private void LB_ItemCheck(object sender, EventArgs e)
        {
            bool check = !(LB.CheckedItems.IndexOf(LB.SelectedItem) >= 0);
            var lay = (LB.SelectedItem as Layer);
            if (lay != null)
                lay.visible = check;
            FastPBDraw(null);
        }
        private void MeinWindow_Load(object sender, EventArgs e)
        {
            creatWind cw = new creatWind();
            cw.ShowDialog();
            if (cw.DialogResult != DialogResult.OK)
            {
                cw.Close();
                Close();
            }
            bmpsize.X = (int)cw.NUDWidht.Value;
            bmpsize.Y = (int)cw.NUDHeight.Value;
            prevBmp = new Bitmap(bmpsize.X, bmpsize.Y);
            nextBmp = new Bitmap(bmpsize.X, bmpsize.Y);
            FBMP = new Bitmap(bmpsize.X, bmpsize.Y);
            Layer background = new Layer(bmpsize.X, bmpsize.Y, OldMod);
            background.name = "Фон";
            background.visible = true;
            LB.Items.Add(background);
            LB.SetItemChecked(0, true);
            background.pencil.FColor = Col.BackColor;
            background.pencil.pensize = TB.Value;
            background.bmp = new Bitmap(bmpsize.X, bmpsize.Y);
            BitmapImage tmpbmp = ExtensionMethods.MyExtensions.ToBitmapImage(background.bmp); // для коректної роботи з фото збереження та закриття
            background.bmp = ExtensionMethods.MyExtensions.BitmapImage2Bitmap(tmpbmp);
            background.pencil.brush.Color = Color.Transparent;
            if (cw.BCol.BackColor != Color.Transparent && !cw.Flag)
            {
                background.pencil.brush.Color = cw.BCol.BackColor;
                Graphics.FromImage(background.bmp).FillRectangle(background.pencil.brush, 0, 0, bmpsize.X, bmpsize.Y);
            }
            else { background.bmp = new Bitmap(bmpsize.X, bmpsize.Y); }
            if (cw.Flag)
                background.bmp = new Bitmap(cw.bm);
            LB.SelectedIndex = 0;
            PenBtn.BackColor = Color.Gray;
            PB.Width = bmpsize.X;
            PB.Height = bmpsize.Y;
            PB.SizeChanged += PB_SizeChanged;
            PB.Width = bmpsize.X;
            PB.Height = bmpsize.Y;
            Size = new Size(Size.Width + 1, Size.Height);
            LB.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(LB_ItemCheck);
        }
        private void Res()
        {
            int x, y, xc, yc;
            PB.Width = Size.Width - 67 - LB.Size.Width - 25; // розмір холста в залежності від зміни вікна
            PB.Height = Size.Height - 80 - 60 - StatusStrip.Size.Height;

            xc = PB.Width / 32;
            yc = PB.Height / 32;
            if (PB.Width > 1 && PB.Height > 1)
                bgbmp = new Bitmap((int)PB.Width, (int)PB.Height);

            var gr = Graphics.FromImage(bgbmp);
            for (x = 0; x <= xc; x++)
                for (y = 0; y <= yc; y++)
                {
                    gr.DrawImage(Mein_Wind.Properties.Resources.transparent, new Rectangle(x * 32, y * 32, 32, 32));
                }
        }
        private void MeinWindow_Resize(object sender, EventArgs e)
        {
            Res();
        }
        private void SizeCH()
        {
            PBScale.X = 1 / ((float)PB.Width / (float)bmpsize.X); // зміна скейла
            PBScale.Y = 1 / ((float)PB.Height / (float)bmpsize.Y);
            // Зміна параметрів скейлів для всіх слоїв
            for (int i = 0; i < LB.Items.Count; i++)
            {
                var layer = LB.Items[i] as Layer;                
                layer.scaleX = 1 / ((float)PB.Width / (float)layer.bmp.Width);
                layer.scaleY = 1 / ((float)PB.Height / (float)layer.bmp.Height);
            }
        }
        private void PB_SizeChanged(object sender, EventArgs e)
        {
            SizeCH();
        }
        private void LB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2) { RenameLayer(); }
            if (e.KeyCode == Keys.Delete) { DeleteLayer(); }
            if (e.KeyCode == Keys.Insert) { AddLayer(); }
            if (e.KeyCode == Keys.Up && e.Control) { e.SuppressKeyPress=true; UpLayer(); }
            if (e.KeyCode == Keys.Down && e.Control) { e.SuppressKeyPress=true; DownLayer(); }
            if (e.KeyCode == Keys.C && e.Control) { e.SuppressKeyPress = true; CopyLay(); }
            FastPBDraw(null);
        }
        private void CopyLay()
        {
            var lay = (LB.SelectedItem as Layer);
            var newLay = new Layer(bmpsize.X,bmpsize.Y, OldMod);
            LB.BeginUpdate();
            newLay.name = "Копія";
            newLay.scaleX = PBScale.X; // зміна скейла
            newLay.scaleY = PBScale.Y;
            LB.Items.Insert(0, newLay);
            LB.SelectedIndex = 0;
            LB.SetItemChecked(0, true);
            LB.EndUpdate();
            newLay.pencil.FColor = Col.BackColor;
            newLay.pencil.pensize = TB.Value;
            newLay.visible = true;
            newLay.bmp = new Bitmap(lay.bmp);
        }
        private void BBleak_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.FColor = BBleak.BackColor;
            Col.BackColor = BBleak.BackColor;
        }

        private void BRed_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.FColor = BRed.BackColor;
            Col.BackColor = BRed.BackColor;
        }

        private void BOrange_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.FColor = BOrange.BackColor;
            Col.BackColor = BOrange.BackColor;
        }

        private void BYell_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.FColor = BYell.BackColor;
            Col.BackColor = BYell.BackColor;
        }

        private void BGreen_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.FColor = BGreen.BackColor;
            Col.BackColor = BGreen.BackColor;
        }

        private void BBlue_Click(object sender, EventArgs e)
        {
            (LB.SelectedItem as Layer).pencil.FColor = BBlue.BackColor;
            Col.BackColor = BBlue.BackColor;
        }

        private void RenameLayer()
        {
            // Перевірка на введення назви слою
            if (LB.SelectedItem == null)
                return;
            var lay = (LB.SelectedItem as Layer);
            string namelayer = lay.name;
            do
            {
                namelayer = Interaction.InputBox("Нове ім'я шару", "Ім'я шару", lay.name);

                if (namelayer == "" || namelayer == " ")
                {
                    if (MessageBox.Show("Не вказано ім'я слою", "Помилка", MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
                        namelayer =lay.name;
                    else
                        namelayer= "";
                }
                else
                    lay.name = namelayer;
            } while (namelayer == "" || namelayer == " ");
            LB.Items[LB.SelectedIndex] = lay;
        }
    }
}

namespace ExtensionMethods
{
    public static class MyExtensions
    {
        public static BitmapImage ToBitmapImage(this Bitmap bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
                return bitmapImage;
            }
        }
        public static Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                return new Bitmap(bitmap);
            }
        }
    }

}
