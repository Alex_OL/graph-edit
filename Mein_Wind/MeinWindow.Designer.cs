﻿
namespace Mein_Wind
{
    partial class MeinWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeinWindow));
            this.UpMenu = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.відкритиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зберегтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зберегтиЯкToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Col = new System.Windows.Forms.Button();
            this.ColDialog = new System.Windows.Forms.ColorDialog();
            this.OP = new System.Windows.Forms.NumericUpDown();
            this.SIZEP = new System.Windows.Forms.NumericUpDown();
            this.TB = new System.Windows.Forms.TrackBar();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.VTimer = new System.Windows.Forms.Timer(this.components);
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.LB = new System.Windows.Forms.CheckedListBox();
            this.LineBtn = new System.Windows.Forms.Button();
            this.PB = new System.Windows.Forms.PictureBox();
            this.B_MoveBtn = new System.Windows.Forms.Button();
            this.ElipseBtn = new System.Windows.Forms.Button();
            this.RectangBtn = new System.Windows.Forms.Button();
            this.FillBtn = new System.Windows.Forms.Button();
            this.RaberBtn = new System.Windows.Forms.Button();
            this.PenBtn = new System.Windows.Forms.Button();
            this.ButDel = new System.Windows.Forms.Button();
            this.B_Down = new System.Windows.Forms.Button();
            this.B_UP = new System.Windows.Forms.Button();
            this.ADD = new System.Windows.Forms.Button();
            this.ToolName = new System.Windows.Forms.ToolTip(this.components);
            this.BBleak = new System.Windows.Forms.Button();
            this.BRed = new System.Windows.Forms.Button();
            this.BYell = new System.Windows.Forms.Button();
            this.BOrange = new System.Windows.Forms.Button();
            this.BBlue = new System.Windows.Forms.Button();
            this.BGreen = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.UpMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SIZEP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // UpMenu
            // 
            this.UpMenu.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.UpMenu.Font = new System.Drawing.Font("Arial", 9F);
            this.UpMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.UpMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.UpMenu.Location = new System.Drawing.Point(0, 0);
            this.UpMenu.Name = "UpMenu";
            this.UpMenu.Size = new System.Drawing.Size(1253, 25);
            this.UpMenu.TabIndex = 10;
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.відкритиToolStripMenuItem,
            this.зберегтиToolStripMenuItem,
            this.зберегтиЯкToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(57, 21);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // відкритиToolStripMenuItem
            // 
            this.відкритиToolStripMenuItem.Name = "відкритиToolStripMenuItem";
            this.відкритиToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
            this.відкритиToolStripMenuItem.Text = "Відкрити";
            this.відкритиToolStripMenuItem.Click += new System.EventHandler(this.відкритиToolStripMenuItem_Click);
            // 
            // зберегтиToolStripMenuItem
            // 
            this.зберегтиToolStripMenuItem.Name = "зберегтиToolStripMenuItem";
            this.зберегтиToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
            this.зберегтиToolStripMenuItem.Text = "Зберегти";
            this.зберегтиToolStripMenuItem.Click += new System.EventHandler(this.зберегтиToolStripMenuItem_Click);
            // 
            // зберегтиЯкToolStripMenuItem
            // 
            this.зберегтиЯкToolStripMenuItem.Name = "зберегтиЯкToolStripMenuItem";
            this.зберегтиЯкToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
            this.зберегтиЯкToolStripMenuItem.Text = "Зберегти як";
            this.зберегтиЯкToolStripMenuItem.Click += new System.EventHandler(this.зберегтиЯкToolStripMenuItem_Click);
            // 
            // Col
            // 
            this.Col.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Col.BackColor = System.Drawing.Color.Black;
            this.Col.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Col.Location = new System.Drawing.Point(835, 41);
            this.Col.Name = "Col";
            this.Col.Size = new System.Drawing.Size(50, 50);
            this.Col.TabIndex = 12;
            this.ToolName.SetToolTip(this.Col, "Палітра кольорів");
            this.Col.UseVisualStyleBackColor = false;
            this.Col.Click += new System.EventHandler(this.Col_Click);
            // 
            // OP
            // 
            this.OP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.OP.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OP.Location = new System.Drawing.Point(1123, 41);
            this.OP.Name = "OP";
            this.OP.Size = new System.Drawing.Size(118, 38);
            this.OP.TabIndex = 13;
            this.OP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.OP.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.OP.ValueChanged += new System.EventHandler(this.OP_ValueChanged);
            // 
            // SIZEP
            // 
            this.SIZEP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SIZEP.Font = new System.Drawing.Font("Arial Narrow", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SIZEP.Location = new System.Drawing.Point(243, 46);
            this.SIZEP.Name = "SIZEP";
            this.SIZEP.Size = new System.Drawing.Size(74, 34);
            this.SIZEP.TabIndex = 15;
            this.SIZEP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ToolName.SetToolTip(this.SIZEP, "Розмір олівця");
            this.SIZEP.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.SIZEP.ValueChanged += new System.EventHandler(this.SIZEP_ValueChanged);
            // 
            // TB
            // 
            this.TB.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.TB.Location = new System.Drawing.Point(12, 39);
            this.TB.Maximum = 100;
            this.TB.Minimum = 1;
            this.TB.Name = "TB";
            this.TB.Size = new System.Drawing.Size(225, 56);
            this.TB.TabIndex = 16;
            this.TB.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.ToolName.SetToolTip(this.TB, "Розмір олівця");
            this.TB.Value = 3;
            this.TB.ValueChanged += new System.EventHandler(this.TB_ValueChanged);
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(916, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 29);
            this.label1.TabIndex = 18;
            this.label1.Text = "Прозорість шару";
            // 
            // SFD
            // 
            this.SFD.AddExtension = global::Mein_Wind.Properties.Settings.Default.AddExtention;
            this.SFD.CreatePrompt = true;
            this.SFD.OverwritePrompt = global::Mein_Wind.Properties.Settings.Default.OverwritePrompt;
            // 
            // VTimer
            // 
            this.VTimer.Enabled = true;
            this.VTimer.Interval = 1;
            this.VTimer.Tick += new System.EventHandler(this.VTimer_Tick);
            // 
            // StatusStrip
            // 
            this.StatusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.StatusStrip.Location = new System.Drawing.Point(0, 688);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.StatusStrip.Size = new System.Drawing.Size(1253, 22);
            this.StatusStrip.TabIndex = 26;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // LB
            // 
            this.LB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LB.Font = new System.Drawing.Font("Arial Narrow", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LB.FormattingEnabled = true;
            this.LB.Location = new System.Drawing.Point(921, 245);
            this.LB.Name = "LB";
            this.LB.Size = new System.Drawing.Size(326, 437);
            this.LB.TabIndex = 28;
            this.ToolName.SetToolTip(this.LB, "F2  - Перейменувати шар\r\nCtrl + C - Скопіювати шар ");
            this.LB.Click += new System.EventHandler(this.LB_Click);
            this.LB.SelectedIndexChanged += new System.EventHandler(this.LB_SelectedIndexChanged);
            this.LB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LB_KeyDown);
            // 
            // LineBtn
            // 
            this.LineBtn.BackgroundImage = global::Mein_Wind.Properties.Resources.Line_;
            this.LineBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.LineBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LineBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.LineBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.LineBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.LineBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.LineBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LineBtn.Location = new System.Drawing.Point(0, 454);
            this.LineBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LineBtn.Name = "LineBtn";
            this.LineBtn.Size = new System.Drawing.Size(83, 56);
            this.LineBtn.TabIndex = 29;
            this.ToolName.SetToolTip(this.LineBtn, "Лінія");
            this.LineBtn.UseVisualStyleBackColor = true;
            this.LineBtn.Click += new System.EventHandler(this.LineBtn_Click);
            // 
            // PB
            // 
            this.PB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PB.Cursor = System.Windows.Forms.Cursors.Cross;
            this.PB.Location = new System.Drawing.Point(89, 100);
            this.PB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PB.Name = "PB";
            this.PB.Size = new System.Drawing.Size(813, 553);
            this.PB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB.TabIndex = 27;
            this.PB.TabStop = false;
            this.PB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PB_MouseDown);
            this.PB.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PB_MouseMove);
            this.PB.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PB_MouseUp);
            // 
            // B_MoveBtn
            // 
            this.B_MoveBtn.BackgroundImage = global::Mein_Wind.Properties.Resources.Move_;
            this.B_MoveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.B_MoveBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.B_MoveBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.B_MoveBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.B_MoveBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.B_MoveBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.B_MoveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B_MoveBtn.Location = new System.Drawing.Point(0, 100);
            this.B_MoveBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.B_MoveBtn.Name = "B_MoveBtn";
            this.B_MoveBtn.Size = new System.Drawing.Size(83, 56);
            this.B_MoveBtn.TabIndex = 25;
            this.ToolName.SetToolTip(this.B_MoveBtn, "Переміщення");
            this.B_MoveBtn.UseVisualStyleBackColor = false;
            this.B_MoveBtn.Click += new System.EventHandler(this.B_Move_Click);
            // 
            // ElipseBtn
            // 
            this.ElipseBtn.BackgroundImage = global::Mein_Wind.Properties.Resources.Ellipse_;
            this.ElipseBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ElipseBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ElipseBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ElipseBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.ElipseBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.ElipseBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.ElipseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ElipseBtn.Location = new System.Drawing.Point(0, 394);
            this.ElipseBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ElipseBtn.Name = "ElipseBtn";
            this.ElipseBtn.Size = new System.Drawing.Size(83, 56);
            this.ElipseBtn.TabIndex = 24;
            this.ToolName.SetToolTip(this.ElipseBtn, "Круг");
            this.ElipseBtn.UseVisualStyleBackColor = true;
            this.ElipseBtn.Click += new System.EventHandler(this.Elipse_Click);
            // 
            // RectangBtn
            // 
            this.RectangBtn.BackgroundImage = global::Mein_Wind.Properties.Resources.Rectangle_;
            this.RectangBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.RectangBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RectangBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.RectangBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.RectangBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.RectangBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.RectangBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RectangBtn.Location = new System.Drawing.Point(0, 334);
            this.RectangBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RectangBtn.Name = "RectangBtn";
            this.RectangBtn.Size = new System.Drawing.Size(83, 56);
            this.RectangBtn.TabIndex = 23;
            this.ToolName.SetToolTip(this.RectangBtn, "Квардат");
            this.RectangBtn.UseVisualStyleBackColor = true;
            this.RectangBtn.Click += new System.EventHandler(this.Rectang_Click);
            // 
            // FillBtn
            // 
            this.FillBtn.BackgroundImage = global::Mein_Wind.Properties.Resources.Fill_;
            this.FillBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.FillBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FillBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FillBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.FillBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.FillBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.FillBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FillBtn.Location = new System.Drawing.Point(0, 280);
            this.FillBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FillBtn.Name = "FillBtn";
            this.FillBtn.Size = new System.Drawing.Size(83, 50);
            this.FillBtn.TabIndex = 22;
            this.ToolName.SetToolTip(this.FillBtn, "Заливка");
            this.FillBtn.UseVisualStyleBackColor = true;
            this.FillBtn.Click += new System.EventHandler(this.FILL_Click);
            // 
            // RaberBtn
            // 
            this.RaberBtn.BackgroundImage = global::Mein_Wind.Properties.Resources.Raber_;
            this.RaberBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.RaberBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RaberBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.RaberBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.RaberBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.RaberBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.RaberBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RaberBtn.Location = new System.Drawing.Point(0, 220);
            this.RaberBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RaberBtn.Name = "RaberBtn";
            this.RaberBtn.Size = new System.Drawing.Size(83, 56);
            this.RaberBtn.TabIndex = 21;
            this.ToolName.SetToolTip(this.RaberBtn, "Гумка");
            this.RaberBtn.UseVisualStyleBackColor = true;
            this.RaberBtn.Click += new System.EventHandler(this.Raber_Click);
            // 
            // PenBtn
            // 
            this.PenBtn.BackgroundImage = global::Mein_Wind.Properties.Resources.pensile_;
            this.PenBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PenBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PenBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.PenBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.PenBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.PenBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PenBtn.Location = new System.Drawing.Point(0, 160);
            this.PenBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PenBtn.Name = "PenBtn";
            this.PenBtn.Size = new System.Drawing.Size(83, 56);
            this.PenBtn.TabIndex = 20;
            this.ToolName.SetToolTip(this.PenBtn, "Олівець");
            this.PenBtn.UseVisualStyleBackColor = true;
            this.PenBtn.Click += new System.EventHandler(this.Pen_Click);
            // 
            // ButDel
            // 
            this.ButDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButDel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ButDel.BackgroundImage = global::Mein_Wind.Properties.Resources.LayerDest_;
            this.ButDel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButDel.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ButDel.Location = new System.Drawing.Point(921, 101);
            this.ButDel.Name = "ButDel";
            this.ButDel.Size = new System.Drawing.Size(153, 59);
            this.ButDel.TabIndex = 19;
            this.ToolName.SetToolTip(this.ButDel, "Видалити шар\r\nабо Delete");
            this.ButDel.UseVisualStyleBackColor = false;
            this.ButDel.Click += new System.EventHandler(this.ButDel_Click);
            // 
            // B_Down
            // 
            this.B_Down.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.B_Down.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.B_Down.BackgroundImage = global::Mein_Wind.Properties.Resources.LayerDown_;
            this.B_Down.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.B_Down.Cursor = System.Windows.Forms.Cursors.Hand;
            this.B_Down.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.B_Down.Location = new System.Drawing.Point(1080, 168);
            this.B_Down.Name = "B_Down";
            this.B_Down.Size = new System.Drawing.Size(167, 59);
            this.B_Down.TabIndex = 11;
            this.ToolName.SetToolTip(this.B_Down, "Перемістити шар до низу \r\nабо Ctrl + ↓");
            this.B_Down.UseVisualStyleBackColor = false;
            this.B_Down.Click += new System.EventHandler(this.B_Down_Click);
            // 
            // B_UP
            // 
            this.B_UP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.B_UP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.B_UP.BackgroundImage = global::Mein_Wind.Properties.Resources.LayerUP_;
            this.B_UP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.B_UP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.B_UP.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.B_UP.Location = new System.Drawing.Point(1080, 101);
            this.B_UP.Name = "B_UP";
            this.B_UP.Size = new System.Drawing.Size(167, 59);
            this.B_UP.TabIndex = 3;
            this.ToolName.SetToolTip(this.B_UP, "Перемістити шар до верху \r\nабо Ctrl + ↑");
            this.B_UP.UseVisualStyleBackColor = false;
            this.B_UP.Click += new System.EventHandler(this.B_UP_Click);
            // 
            // ADD
            // 
            this.ADD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ADD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ADD.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ADD.BackgroundImage")));
            this.ADD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ADD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ADD.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ADD.Location = new System.Drawing.Point(921, 168);
            this.ADD.Name = "ADD";
            this.ADD.Size = new System.Drawing.Size(153, 59);
            this.ADD.TabIndex = 2;
            this.ToolName.SetToolTip(this.ADD, "Додати шар\r\nабо Insert");
            this.ADD.UseVisualStyleBackColor = false;
            this.ADD.Click += new System.EventHandler(this.ADD_Click);
            // 
            // BBleak
            // 
            this.BBleak.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BBleak.BackColor = System.Drawing.Color.Black;
            this.BBleak.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BBleak.Location = new System.Drawing.Point(39, 8);
            this.BBleak.Name = "BBleak";
            this.BBleak.Size = new System.Drawing.Size(50, 50);
            this.BBleak.TabIndex = 30;
            this.ToolName.SetToolTip(this.BBleak, "Колір Чорний");
            this.BBleak.UseVisualStyleBackColor = false;
            this.BBleak.Click += new System.EventHandler(this.BBleak_Click);
            // 
            // BRed
            // 
            this.BRed.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BRed.BackColor = System.Drawing.Color.Red;
            this.BRed.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BRed.Location = new System.Drawing.Point(110, 8);
            this.BRed.Name = "BRed";
            this.BRed.Size = new System.Drawing.Size(50, 50);
            this.BRed.TabIndex = 31;
            this.ToolName.SetToolTip(this.BRed, "Колір Червоний");
            this.BRed.UseVisualStyleBackColor = false;
            this.BRed.Click += new System.EventHandler(this.BRed_Click);
            // 
            // BYell
            // 
            this.BYell.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BYell.BackColor = System.Drawing.Color.Yellow;
            this.BYell.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BYell.Location = new System.Drawing.Point(258, 8);
            this.BYell.Name = "BYell";
            this.BYell.Size = new System.Drawing.Size(50, 50);
            this.BYell.TabIndex = 33;
            this.ToolName.SetToolTip(this.BYell, "Колір Жовтий");
            this.BYell.UseVisualStyleBackColor = false;
            this.BYell.Click += new System.EventHandler(this.BYell_Click);
            // 
            // BOrange
            // 
            this.BOrange.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BOrange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BOrange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BOrange.Location = new System.Drawing.Point(183, 8);
            this.BOrange.Name = "BOrange";
            this.BOrange.Size = new System.Drawing.Size(50, 50);
            this.BOrange.TabIndex = 32;
            this.ToolName.SetToolTip(this.BOrange, "Колір Оранжевий");
            this.BOrange.UseVisualStyleBackColor = false;
            this.BOrange.Click += new System.EventHandler(this.BOrange_Click);
            // 
            // BBlue
            // 
            this.BBlue.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BBlue.BackColor = System.Drawing.Color.Blue;
            this.BBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BBlue.Location = new System.Drawing.Point(398, 8);
            this.BBlue.Name = "BBlue";
            this.BBlue.Size = new System.Drawing.Size(50, 50);
            this.BBlue.TabIndex = 35;
            this.ToolName.SetToolTip(this.BBlue, "Колір синій");
            this.BBlue.UseVisualStyleBackColor = false;
            this.BBlue.Click += new System.EventHandler(this.BBlue_Click);
            // 
            // BGreen
            // 
            this.BGreen.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BGreen.BackColor = System.Drawing.Color.Lime;
            this.BGreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BGreen.Location = new System.Drawing.Point(330, 8);
            this.BGreen.Name = "BGreen";
            this.BGreen.Size = new System.Drawing.Size(50, 50);
            this.BGreen.TabIndex = 34;
            this.ToolName.SetToolTip(this.BGreen, "Колір Зелений");
            this.BGreen.UseVisualStyleBackColor = false;
            this.BGreen.Click += new System.EventHandler(this.BGreen_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.BBlue);
            this.panel1.Controls.Add(this.BBleak);
            this.panel1.Controls.Add(this.BGreen);
            this.panel1.Controls.Add(this.BRed);
            this.panel1.Controls.Add(this.BYell);
            this.panel1.Controls.Add(this.BOrange);
            this.panel1.Location = new System.Drawing.Point(334, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(495, 64);
            this.panel1.TabIndex = 36;
            // 
            // MeinWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(1253, 710);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LineBtn);
            this.Controls.Add(this.LB);
            this.Controls.Add(this.PB);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.B_MoveBtn);
            this.Controls.Add(this.ElipseBtn);
            this.Controls.Add(this.RectangBtn);
            this.Controls.Add(this.FillBtn);
            this.Controls.Add(this.RaberBtn);
            this.Controls.Add(this.PenBtn);
            this.Controls.Add(this.ButDel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB);
            this.Controls.Add(this.SIZEP);
            this.Controls.Add(this.OP);
            this.Controls.Add(this.Col);
            this.Controls.Add(this.B_Down);
            this.Controls.Add(this.B_UP);
            this.Controls.Add(this.ADD);
            this.Controls.Add(this.UpMenu);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.UpMenu;
            this.MinimumSize = new System.Drawing.Size(1271, 757);
            this.Name = "MeinWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Draw Edit";
            this.Load += new System.EventHandler(this.MeinWindow_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.Resize += new System.EventHandler(this.MeinWindow_Resize);
            this.UpMenu.ResumeLayout(false);
            this.UpMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SIZEP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button ADD;
        private System.Windows.Forms.Button B_UP;
        private System.Windows.Forms.MenuStrip UpMenu;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem відкритиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem зберегтиToolStripMenuItem;
        private System.Windows.Forms.Button B_Down;
        private System.Windows.Forms.Button Col;
        private System.Windows.Forms.ColorDialog ColDialog;
        private System.Windows.Forms.NumericUpDown OP;
        private System.Windows.Forms.NumericUpDown SIZEP;
        private System.Windows.Forms.TrackBar TB;
        private System.Windows.Forms.ToolStripMenuItem зберегтиЯкToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButDel;
        private System.Windows.Forms.Timer VTimer;
        private System.Windows.Forms.Button ElipseBtn;
        private System.Windows.Forms.Button RectangBtn;
        private System.Windows.Forms.Button FillBtn;
        private System.Windows.Forms.Button RaberBtn;
        private System.Windows.Forms.Button PenBtn;
        private System.Windows.Forms.Button B_MoveBtn;
        private System.Windows.Forms.StatusStrip StatusStrip;
        public System.Windows.Forms.PictureBox PB;
        private System.Windows.Forms.CheckedListBox LB;
        private System.Windows.Forms.Button LineBtn;
        private System.Windows.Forms.ToolTip ToolName;
        private System.Windows.Forms.Button BBleak;
        private System.Windows.Forms.Button BRed;
        private System.Windows.Forms.Button BYell;
        private System.Windows.Forms.Button BOrange;
        private System.Windows.Forms.Button BBlue;
        private System.Windows.Forms.Button BGreen;
        private System.Windows.Forms.Panel panel1;
    }
}

