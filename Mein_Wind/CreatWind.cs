﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mein_Wind
{
    public partial class creatWind : Form
    {
        public bool Flag;
        public Bitmap bm;
        public creatWind()
        {
            InitializeComponent();
            Flag = false;
        }

        private void NUDWidght_ValueChanged(object sender, EventArgs e)
        {
            if (NUDWidht.Value < 1)
            {
                NUDWidht.Value = 1;
                MessageBox.Show("Помилка, значення має бути >0 ", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            LParam.Text = $"{NUDWidht.Value}x{NUDHeight.Value}"; //Показ зміни розмірів
        }

        private void NUDHeight_ValueChanged(object sender, EventArgs e)
        {
            if (NUDHeight.Value < 1)
            {
                NUDHeight.Value = 1;
                MessageBox.Show("Помилка, значення має бути >0 ", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            LParam.Text = $"{NUDWidht.Value}x{NUDHeight.Value}"; //Показ зміни розмірів
        }

        private void B_Create_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK; //Кнопка переходу до основного вікна
        }

        private void B_Close_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel; //Кнопка закриття вікна
        }

        private void BCol_Click(object sender, EventArgs e)
        {
            if (ColDialog.ShowDialog() == DialogResult.OK)
            {
                //Кнопка зміни кольору
                ((Button)sender).BackColor = ColDialog.Color;
                ((Button)sender).BackgroundImage = null;
            }
        }

        private void BCol_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //Кнопка зміни колору на прозорість
                ((Button)sender).BackColor = Color.Transparent;
                ((Button)sender).BackgroundImage = Mein_Wind.Properties.Resources.transparent;
            }
        }

        private void creatWind_Load(object sender, EventArgs e)
        {
            BCol.BackColor = Color.Transparent;
        }

        private void PB_720x400_Click(object sender, EventArgs e)
        {
            NUDWidht.Value = 720;
            NUDHeight.Value = 400;
        }
        private void LB_720x400_Click(object sender, EventArgs e)
        {
            NUDWidht.Value = 720;
            NUDHeight.Value = 400;
        }

        private void PB640_480_Click(object sender, EventArgs e)
        {
            NUDWidht.Value = 640;
            NUDHeight.Value = 480;
        }
        private void LB_640x480_Click(object sender, EventArgs e)
        {
            NUDWidht.Value = 640;
            NUDHeight.Value = 480;
        }
        private void PB_145x145_Click(object sender, EventArgs e)
        {
            NUDWidht.Value = 145;
            NUDHeight.Value = 145;
        }

        private void LB_145x145_Click(object sender, EventArgs e)
        {
            NUDWidht.Value = 145;
            NUDHeight.Value = 145;
        }

        private void PB_1280x720_Click(object sender, EventArgs e)
        {
            NUDWidht.Value = 1280;
            NUDHeight.Value = 720;
        }

        private void LB_1280x720_Click(object sender, EventArgs e)
        {
            NUDWidht.Value = 1280;
            NUDHeight.Value = 720;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (OpnFD.ShowDialog() == DialogResult.OK)
            {
                Flag = true;
                bm = new Bitmap(OpnFD.FileName);
                NUDWidht.Value = bm.Width;
                NUDHeight.Value = bm.Height;
                DialogResult = DialogResult.OK;
            }
        }
    }
}
