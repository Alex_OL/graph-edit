﻿
namespace Mein_Wind
{
    partial class creatWind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(creatWind));
            this.NUDHeight = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.B_Create = new System.Windows.Forms.Button();
            this.B_Close = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.LB_ColFon = new System.Windows.Forms.Label();
            this.LParam = new System.Windows.Forms.Label();
            this.BCol = new System.Windows.Forms.Button();
            this.NUDWidht = new System.Windows.Forms.NumericUpDown();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ColDialog = new System.Windows.Forms.ColorDialog();
            this.LB_1280x720 = new System.Windows.Forms.Label();
            this.PB_1280x720 = new System.Windows.Forms.PictureBox();
            this.LB_720x400 = new System.Windows.Forms.Label();
            this.PB_720x400 = new System.Windows.Forms.PictureBox();
            this.LB_145x145 = new System.Windows.Forms.Label();
            this.PB_145x145 = new System.Windows.Forms.PictureBox();
            this.LB_640x480 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textGolovnaPanel = new System.Windows.Forms.Panel();
            this.PB640_480 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LBNameHello = new System.Windows.Forms.Label();
            this.MeinPanel = new System.Windows.Forms.Panel();
            this.PanelPruklad = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ToolTop = new System.Windows.Forms.ToolTip(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.OpnFD = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.NUDHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDWidht)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_1280x720)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_720x400)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_145x145)).BeginInit();
            this.textGolovnaPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB640_480)).BeginInit();
            this.panel1.SuspendLayout();
            this.MeinPanel.SuspendLayout();
            this.PanelPruklad.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // NUDHeight
            // 
            this.NUDHeight.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NUDHeight.Location = new System.Drawing.Point(195, 322);
            this.NUDHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NUDHeight.Name = "NUDHeight";
            this.NUDHeight.Size = new System.Drawing.Size(91, 30);
            this.NUDHeight.TabIndex = 4;
            this.NUDHeight.Value = new decimal(new int[] {
            480,
            0,
            0,
            0});
            this.NUDHeight.ValueChanged += new System.EventHandler(this.NUDHeight_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(84, 279);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 27);
            this.label1.TabIndex = 5;
            this.label1.Text = "Штрина:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(88, 328);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 27);
            this.label2.TabIndex = 6;
            this.label2.Text = "Висота:";
            // 
            // B_Create
            // 
            this.B_Create.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.B_Create.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B_Create.Font = new System.Drawing.Font("Arial Narrow", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B_Create.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.B_Create.Location = new System.Drawing.Point(13, 97);
            this.B_Create.Name = "B_Create";
            this.B_Create.Size = new System.Drawing.Size(144, 48);
            this.B_Create.TabIndex = 7;
            this.B_Create.Text = "Створити";
            this.B_Create.UseVisualStyleBackColor = false;
            this.B_Create.Click += new System.EventHandler(this.B_Create_Click);
            // 
            // B_Close
            // 
            this.B_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.B_Close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.B_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B_Close.Font = new System.Drawing.Font("Arial Narrow", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B_Close.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.B_Close.Location = new System.Drawing.Point(13, 238);
            this.B_Close.Name = "B_Close";
            this.B_Close.Size = new System.Drawing.Size(144, 48);
            this.B_Close.TabIndex = 8;
            this.B_Close.Text = "Закрити";
            this.B_Close.UseVisualStyleBackColor = false;
            this.B_Close.Click += new System.EventHandler(this.B_Close_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(85, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 29);
            this.label3.TabIndex = 9;
            this.label3.Text = "Параметри слою:";
            // 
            // LB_ColFon
            // 
            this.LB_ColFon.AutoSize = true;
            this.LB_ColFon.BackColor = System.Drawing.Color.Transparent;
            this.LB_ColFon.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LB_ColFon.ForeColor = System.Drawing.Color.White;
            this.LB_ColFon.Location = new System.Drawing.Point(12, 389);
            this.LB_ColFon.Name = "LB_ColFon";
            this.LB_ColFon.Size = new System.Drawing.Size(238, 27);
            this.LB_ColFon.TabIndex = 11;
            this.LB_ColFon.Text = "Колір заливки фону:";
            // 
            // LParam
            // 
            this.LParam.AutoSize = true;
            this.LParam.BackColor = System.Drawing.Color.Black;
            this.LParam.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LParam.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LParam.Location = new System.Drawing.Point(143, 223);
            this.LParam.Name = "LParam";
            this.LParam.Size = new System.Drawing.Size(73, 24);
            this.LParam.TabIndex = 15;
            this.LParam.Text = "640x480";
            // 
            // BCol
            // 
            this.BCol.BackColor = System.Drawing.Color.White;
            this.BCol.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BCol.BackgroundImage")));
            this.BCol.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BCol.Location = new System.Drawing.Point(263, 372);
            this.BCol.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BCol.Name = "BCol";
            this.BCol.Size = new System.Drawing.Size(60, 60);
            this.BCol.TabIndex = 16;
            this.ToolTop.SetToolTip(this.BCol, "для прозорості натисніть\r\nправу кнопку миші");
            this.BCol.UseVisualStyleBackColor = false;
            this.BCol.Click += new System.EventHandler(this.BCol_Click);
            this.BCol.MouseUp += new System.Windows.Forms.MouseEventHandler(this.BCol_MouseUp);
            // 
            // NUDWidht
            // 
            this.NUDWidht.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NUDWidht.Location = new System.Drawing.Point(195, 279);
            this.NUDWidht.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NUDWidht.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NUDWidht.Name = "NUDWidht";
            this.NUDWidht.Size = new System.Drawing.Size(91, 30);
            this.NUDWidht.TabIndex = 17;
            this.NUDWidht.Value = new decimal(new int[] {
            640,
            0,
            0,
            0});
            this.NUDWidht.ValueChanged += new System.EventHandler(this.NUDWidght_ValueChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(75, 57);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(211, 191);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            // 
            // LB_1280x720
            // 
            this.LB_1280x720.AutoSize = true;
            this.LB_1280x720.BackColor = System.Drawing.Color.Black;
            this.LB_1280x720.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LB_1280x720.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LB_1280x720.Location = new System.Drawing.Point(296, 401);
            this.LB_1280x720.Name = "LB_1280x720";
            this.LB_1280x720.Size = new System.Drawing.Size(82, 23);
            this.LB_1280x720.TabIndex = 10;
            this.LB_1280x720.Text = "1280x720";
            this.LB_1280x720.Click += new System.EventHandler(this.LB_1280x720_Click);
            // 
            // PB_1280x720
            // 
            this.PB_1280x720.Image = ((System.Drawing.Image)(resources.GetObject("PB_1280x720.Image")));
            this.PB_1280x720.Location = new System.Drawing.Point(245, 248);
            this.PB_1280x720.Name = "PB_1280x720";
            this.PB_1280x720.Size = new System.Drawing.Size(189, 178);
            this.PB_1280x720.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_1280x720.TabIndex = 7;
            this.PB_1280x720.TabStop = false;
            this.PB_1280x720.Click += new System.EventHandler(this.PB_1280x720_Click);
            // 
            // LB_720x400
            // 
            this.LB_720x400.AutoSize = true;
            this.LB_720x400.BackColor = System.Drawing.Color.Black;
            this.LB_720x400.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LB_720x400.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LB_720x400.Location = new System.Drawing.Point(305, 198);
            this.LB_720x400.Name = "LB_720x400";
            this.LB_720x400.Size = new System.Drawing.Size(73, 23);
            this.LB_720x400.TabIndex = 4;
            this.LB_720x400.Text = "720x400";
            this.LB_720x400.Click += new System.EventHandler(this.LB_720x400_Click);
            // 
            // PB_720x400
            // 
            this.PB_720x400.Image = ((System.Drawing.Image)(resources.GetObject("PB_720x400.Image")));
            this.PB_720x400.Location = new System.Drawing.Point(245, 44);
            this.PB_720x400.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PB_720x400.Name = "PB_720x400";
            this.PB_720x400.Size = new System.Drawing.Size(189, 178);
            this.PB_720x400.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_720x400.TabIndex = 1;
            this.PB_720x400.TabStop = false;
            this.PB_720x400.Click += new System.EventHandler(this.PB_720x400_Click);
            // 
            // LB_145x145
            // 
            this.LB_145x145.AutoSize = true;
            this.LB_145x145.BackColor = System.Drawing.Color.Black;
            this.LB_145x145.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LB_145x145.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LB_145x145.Location = new System.Drawing.Point(89, 401);
            this.LB_145x145.Name = "LB_145x145";
            this.LB_145x145.Size = new System.Drawing.Size(73, 23);
            this.LB_145x145.TabIndex = 9;
            this.LB_145x145.Text = "145x145";
            this.LB_145x145.Click += new System.EventHandler(this.LB_145x145_Click);
            // 
            // PB_145x145
            // 
            this.PB_145x145.Image = ((System.Drawing.Image)(resources.GetObject("PB_145x145.Image")));
            this.PB_145x145.Location = new System.Drawing.Point(25, 248);
            this.PB_145x145.Name = "PB_145x145";
            this.PB_145x145.Size = new System.Drawing.Size(189, 178);
            this.PB_145x145.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_145x145.TabIndex = 6;
            this.PB_145x145.TabStop = false;
            this.PB_145x145.Click += new System.EventHandler(this.PB_145x145_Click);
            // 
            // LB_640x480
            // 
            this.LB_640x480.AutoSize = true;
            this.LB_640x480.BackColor = System.Drawing.Color.Black;
            this.LB_640x480.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LB_640x480.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LB_640x480.Location = new System.Drawing.Point(81, 198);
            this.LB_640x480.Name = "LB_640x480";
            this.LB_640x480.Size = new System.Drawing.Size(73, 23);
            this.LB_640x480.TabIndex = 3;
            this.LB_640x480.Text = "640x480";
            this.LB_640x480.Click += new System.EventHandler(this.LB_640x480_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label12.Location = new System.Drawing.Point(29, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 29);
            this.label12.TabIndex = 0;
            this.label12.Text = "Головна";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textGolovnaPanel
            // 
            this.textGolovnaPanel.BackColor = System.Drawing.Color.Gray;
            this.textGolovnaPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textGolovnaPanel.Controls.Add(this.label12);
            this.textGolovnaPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.textGolovnaPanel.Location = new System.Drawing.Point(0, 0);
            this.textGolovnaPanel.Name = "textGolovnaPanel";
            this.textGolovnaPanel.Size = new System.Drawing.Size(185, 64);
            this.textGolovnaPanel.TabIndex = 1;
            // 
            // PB640_480
            // 
            this.PB640_480.Image = ((System.Drawing.Image)(resources.GetObject("PB640_480.Image")));
            this.PB640_480.Location = new System.Drawing.Point(25, 44);
            this.PB640_480.Name = "PB640_480";
            this.PB640_480.Size = new System.Drawing.Size(189, 178);
            this.PB640_480.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB640_480.TabIndex = 0;
            this.PB640_480.TabStop = false;
            this.PB640_480.Click += new System.EventHandler(this.PB640_480_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.LBNameHello);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(187, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(791, 65);
            this.panel1.TabIndex = 20;
            // 
            // LBNameHello
            // 
            this.LBNameHello.AutoSize = true;
            this.LBNameHello.BackColor = System.Drawing.Color.Transparent;
            this.LBNameHello.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBNameHello.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LBNameHello.Location = new System.Drawing.Point(142, 14);
            this.LBNameHello.Name = "LBNameHello";
            this.LBNameHello.Size = new System.Drawing.Size(460, 34);
            this.LBNameHello.TabIndex = 0;
            this.LBNameHello.Text = "Раді бачити вас знову в DrawEdit";
            // 
            // MeinPanel
            // 
            this.MeinPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MeinPanel.Controls.Add(this.button1);
            this.MeinPanel.Controls.Add(this.textGolovnaPanel);
            this.MeinPanel.Controls.Add(this.B_Create);
            this.MeinPanel.Controls.Add(this.B_Close);
            this.MeinPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.MeinPanel.Location = new System.Drawing.Point(0, 0);
            this.MeinPanel.Name = "MeinPanel";
            this.MeinPanel.Size = new System.Drawing.Size(187, 538);
            this.MeinPanel.TabIndex = 19;
            // 
            // PanelPruklad
            // 
            this.PanelPruklad.BackColor = System.Drawing.Color.Silver;
            this.PanelPruklad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPruklad.Controls.Add(this.LB_1280x720);
            this.PanelPruklad.Controls.Add(this.LB_145x145);
            this.PanelPruklad.Controls.Add(this.PB_1280x720);
            this.PanelPruklad.Controls.Add(this.PB_145x145);
            this.PanelPruklad.Controls.Add(this.LB_720x400);
            this.PanelPruklad.Controls.Add(this.LB_640x480);
            this.PanelPruklad.Controls.Add(this.PB_720x400);
            this.PanelPruklad.Controls.Add(this.PB640_480);
            this.PanelPruklad.Location = new System.Drawing.Point(187, 65);
            this.PanelPruklad.Name = "PanelPruklad";
            this.PanelPruklad.Size = new System.Drawing.Size(459, 473);
            this.PanelPruklad.TabIndex = 21;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGray;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.BCol);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.NUDHeight);
            this.panel2.Controls.Add(this.NUDWidht);
            this.panel2.Controls.Add(this.LB_ColFon);
            this.panel2.Controls.Add(this.LParam);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(642, 65);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(336, 473);
            this.panel2.TabIndex = 22;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(13, 169);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 48);
            this.button1.TabIndex = 9;
            this.button1.Text = "Відкрити";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // OpnFD
            // 
            this.OpnFD.FileName = "openFileDialog1";
            // 
            // creatWind
            // 
            this.AcceptButton = this.B_Create;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.CancelButton = this.B_Close;
            this.ClientSize = new System.Drawing.Size(978, 538);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.MeinPanel);
            this.Controls.Add(this.PanelPruklad);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(370, 449);
            this.Name = "creatWind";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Створити документ";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.creatWind_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NUDHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDWidht)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_1280x720)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_720x400)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_145x145)).EndInit();
            this.textGolovnaPanel.ResumeLayout(false);
            this.textGolovnaPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB640_480)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.MeinPanel.ResumeLayout(false);
            this.PanelPruklad.ResumeLayout(false);
            this.PanelPruklad.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button B_Create;
        private System.Windows.Forms.Button B_Close;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LB_ColFon;
        private System.Windows.Forms.Label LParam;
        public System.Windows.Forms.NumericUpDown NUDHeight;
        public System.Windows.Forms.Button BCol;
        public System.Windows.Forms.NumericUpDown NUDWidht;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.ColorDialog ColDialog;
        private System.Windows.Forms.Label LB_1280x720;
        private System.Windows.Forms.PictureBox PB_1280x720;
        private System.Windows.Forms.Label LB_720x400;
        private System.Windows.Forms.PictureBox PB_720x400;
        private System.Windows.Forms.Label LB_145x145;
        private System.Windows.Forms.PictureBox PB_145x145;
        private System.Windows.Forms.Label LB_640x480;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel textGolovnaPanel;
        private System.Windows.Forms.PictureBox PB640_480;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LBNameHello;
        private System.Windows.Forms.Panel MeinPanel;
        private System.Windows.Forms.Panel PanelPruklad;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolTip ToolTop;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.OpenFileDialog OpnFD;
    }
}